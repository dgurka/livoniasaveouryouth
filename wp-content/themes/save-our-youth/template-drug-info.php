<?php
/*
Template Name: Drug Info
*/
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    <link rel="shortcut icon" href="<?php bloginfo('stylesheet_directory'); ?>/favicon.ico" />
</head>
<body>

	<div id="container">
    
    	<div id="logo">
        	<a href="http://www.livoniasaveouryouth.org"><img src="http://www.livoniasaveouryouth.org/wp-content/themes/save-our-youth/images/logo.png" title="Livonia Save Our Youth Coalition" /></a>
        </div><!--/#logo-->
        
        <div id="nav">
        	<ul>
        		<?php wp_list_pages('depth=1&title_li=&exclude=591'); ?>
            </ul>
        </div><!--/#nav-->
        
        <div id="content-wrapper" class="drug-info">
        
            <div id="content-main">
                
                <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>			
                    
                    <?php the_content(); ?>
                    <?php wp_link_pages( array( 'before' => '' . __( 'Pages:', 'twentyten' ), 'after' => '' ) ); ?>
                    <?php edit_post_link( __( 'Edit', 'twentyten' ), '', '' ); ?>
                
                <?php endwhile; ?>
                
            </div><!--/#content-main-->
            
            <div id="content-sub">
                
				<div id="subnav">
					<?php
						if($post->post_parent)
							$children = wp_list_pages("title_li=&child_of=".$post->post_parent."&echo=0"); else
							$children = wp_list_pages("title_li=&child_of=".$post->ID."&echo=0");
						if ($children) { ?>
							<ul>
								<?php echo $children; ?>
							</ul>
						<?php } ?>
				</div><!--/#subnav-->
                
                <div id="secondary-widget" style="clear: both;">
                	<?php if ( ! dynamic_sidebar( 'secondary-widget-area' ) ) : ?><?php endif; ?>
                </div><!--/#secondary-widget(search bar)-->
                
            </div><!--/#content-sub-->
            
        </div><!--/#content-wrapper-->
    
    </div><!--/#container-->
    
    <div id="footer-wide">
    	<div id="footer">
    		<div class="footer-main">
            	<a href="http://www.livoniasaveouryouth.org"><img class="icon" src="http://www.livoniasaveouryouth.org/wp-content/themes/save-our-youth/images/logo-footer.png" title="Livonia Save Our Youth Coalition" /></a><br />
                <p>Livonia City Hall Annex, 33000 Civic Center Drive, Livonia, <abbr title="Michigan">MI</abbr> 48154<br />(734) 388-9580 &bull; <a href="mailto:info@livoniasaveouryouth.org">e-mail</a> &bull; <a href="http://www.livoniasaveouryouth.org/site-map">site map</a> &bull; <a href="http://www.icancreatethat.com" target="_blank" title="Web Design by Maria Gosur">web design</a></p> 
            </div>
            <div class="footer-sub">
            	<a class="btn-green" href="http://www.livoniasaveouryouth.org/programs-and-events" title="Come to an event">Come to an Event</a><br />
                <a class="btn-orange" href="http://www.livoniasaveouryouth.org/programs-and-events/coalition-meetings" title="Join our meetings">Join Our Meetings</a><br />
                <a class="btn-blue" href="http://www.livoniasaveouryouth.org/programs-and-events/call-for-volunteers" title="Volunteer">Volunteer with Us</a>
            </div>
            <div class="footer-sub">
            	<br /><br />
            	<a href="http://www.facebook.com/pages/Livonia-Save-Our-Youth-Task-Force/173106307017" target="_blank"><img class="icon" src="http://www.livoniasaveouryouth.org/wp-content/themes/save-our-youth/images/icon-facebook.png" title="Find us on Facebook" /></a><br />
                <a href="https://twitter.com/#!/lsoyc" target="_blank"><img class="icon" src="http://www.livoniasaveouryouth.org/wp-content/themes/save-our-youth/images/icon-twitter.png" title="Follow us on Twitter" /></a>
            </div>
        </div><!--/#footer-->
    </div><!--/#footer-wide-->

</body>
</html>
 

