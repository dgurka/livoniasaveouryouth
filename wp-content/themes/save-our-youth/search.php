<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.0
 */

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    <link rel="shortcut icon" href="<?php bloginfo('stylesheet_directory'); ?>/favicon.ico" />
</head>
<body>

	<div id="container">
    
    	<div id="logo">
        	<a href="http://www.livoniasaveouryouth.org"><img src="http://www.livoniasaveouryouth.org/wp-content/themes/save-our-youth/images/logo.png" title="Livonia Save Our Youth Coalition" /></a>
        </div><!--/#logo-->
        
        <div id="nav">
        	<ul>
        		<?php wp_list_pages('depth=1&title_li=&exclude=591'); ?>
            </ul>
        </div><!--/#nav-->
        
        <div id="content-wrapper" class="about-us">
        
            <div id="content-main">
            	<h1>Looking for Something?</h1>
                
                <?php if ( have_posts() ) : ?>
				<h2><?php printf( __( 'Search Results for: %s', 'twentyten' ), '' . get_search_query() . '' ); ?></h2><br />
				<?php
				/* Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called loop-search.php and that will be used instead.
				 */
				 get_template_part( 'loop', 'search' );
				?>
				<?php else : ?>
					<h2><?php _e( 'Nothing Found', 'twentyten' ); ?></h2>
					<p><?php _e( 'Sorry, but nothing matched your search criteria. Please try again with some different keywords.', 'twentyten' ); ?></p>
					<?php get_search_form(); ?>
				<?php endif; ?>
                
            </div><!--/#content-main-->
            
            <div id="content-sub">
                
				<div id="subnav">
                	<p>
						<?php
                            $url = wp_get_referer();
                            $path_parts = pathinfo($url);
                            $my_referrer = wp_get_referer();
                            if( $my_referrer ) {
                                echo '<a href="'.$my_referrer .'">&laquo; back to ' . $path_parts['filename'], '</a>';
                            }
                        ?>
                    </p>
				</div><!--/#subnav-->
                
            </div><!--/#content-sub-->
            
        </div><!--/#content-wrapper-->
    
    </div><!--/#container-->
    
    <div id="footer-wide">
    	<div id="footer">
    		<div class="footer-main">
            	<a href="http://www.livoniasaveouryouth.org"><img class="icon" src="http://www.livoniasaveouryouth.org/wp-content/themes/save-our-youth/images/logo-footer.png" title="Livonia Save Our Youth Coalition" /></a><br />
                <p>Suite 102, 8623 Wayne Road &bull; Westland, <abbr title="Michigan">MI</abbr> 48185<br />(734) 388-9580 &bull; <a href="mailto:saveouryouthtaskforce@gmail.com">e-mail</a> &bull; <a href="http://www.livoniasaveouryouth.org/site-map">site map</a> &bull; <a href="http://www.icancreatethat.com" target="_blank" title="Web Design by Maria Gosur">web design</a></p> 
            </div>
            <div class="footer-sub">
            	<a class="btn-green" href="http://www.livoniasaveouryouth.org/programs-and-events" title="Come to an event">Come to an Event</a><br />
                <a class="btn-orange" href="http://www.livoniasaveouryouth.org/programs-and-events/coalition-meetings" title="Join our meetings">Join Our Meetings</a><br />
                <a class="btn-blue" href="http://www.livoniasaveouryouth.org/programs-and-events/call-for-volunteers" title="Volunteer">Volunteer with Us</a>
            </div>
            <div class="footer-sub">
            	<br /><br />
            	<a href="http://www.facebook.com/pages/Livonia-Save-Our-Youth-Task-Force/173106307017" target="_blank"><img class="icon" src="http://www.livoniasaveouryouth.org/wp-content/themes/save-our-youth/images/icon-facebook.png" title="Find us on Facebook" /></a><br />
                <a href="https://twitter.com/#!/lsoytf" target="_blank"><img class="icon" src="http://www.livoniasaveouryouth.org/wp-content/themes/save-our-youth/images/icon-twitter.png" title="Follow us on Twitter" /></a>
            </div>
        </div><!--/#footer-->
    </div><!--/#footer-wide-->

</body>
</html>
