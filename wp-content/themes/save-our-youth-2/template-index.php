<?php
/*
Template Name: Home
*/
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    <link rel="shortcut icon" href="<?php bloginfo('stylesheet_directory'); ?>/favicon.ico" />
</head>
<body>

	<div id="container">
    
    	<div id="logo">
        	<a href="http://www.livoniasaveouryouth.org"><img src="http://www.livoniasaveouryouth.org/wp-content/themes/save-our-youth/images/logo.png" title="Livonia Save Our Youth Coalition" /></a>
        </div><!--/#logo-->
        
        <div id="nav">
        	<ul>
        		<?php wp_list_pages('depth=1&title_li=&exclude=591'); ?>
            </ul>
        </div><!--/#nav-->
        
        <div id="content-wrapper" class="index">
        
            <div id="content-main">
 
                <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>			
                    
                    <?php the_content(); ?>
                    <?php wp_link_pages( array( 'before' => '' . __( 'Pages:', 'twentyten' ), 'after' => '' ) ); ?>
                    <?php edit_post_link( __( 'Edit', 'twentyten' ), '', '' ); ?>
                
                <?php endwhile; ?>
                
            </div><!--/#content-main-->
            
            <div id="content-sub">  
                
                <div id="primary-widget" style="width: 228px; margin-top: -1px; clear: both;">
					<?php if ( ! dynamic_sidebar( 'primary-widget-area' ) ) : ?><?php endif; ?>
                </div><!--/#primary-widget(events calendar)--> 
                
                <div id="secondary-widget" style="clear: both;">
                	<?php if ( ! dynamic_sidebar( 'secondary-widget-area' ) ) : ?><?php endif; ?>
                </div><!--/#secondary-widget(search bar)-->
                
                <br /><br /><br /><br />
                
                <div style="clear: both;">
					<script src="http://widgets.twimg.com/j/2/widget.js"></script>
                    <script>
                        new TWTR.Widget({
                          version: 2,
                          type: 'profile',
                          rpp: 2,
                          interval: 6000,
                          width: 228,
                          height: 100,
                          theme: {
                            shell: {
                              background: '#a9a9a9',
                              color: '#333'
                            },
                            tweets: {
                              background: '#eee',
                              color: '#333',
                              links: '#f6921c'
                            }
                          },
                          features: {
                            scrollbar: false,
                            loop: false,
                            live: false,
                            hashtags: true,
                            timestamp: true,
                            avatars: true,
                            behavior: 'all'
                          }
                        }).render().setUser('lsoyc').start();
                    </script>
                </div>
                
            </div><!--/#content-sub-->
            
        </div><!--/#content-wrapper-->
        
        <div id="footer-home">
            <a class="btn-highlight" href="/programs-and-events" title="Come to an event">Come join us<br /><span class="highlight-fade">at our next event</span></a>
            <a class="btn-highlight" href="/resources" title="Find help and facts with resources">Find help and facts<br /><span class="highlight-fade"> with resources</span></a>
            <a class="btn-highlight" href="http://www.facebook.com/pages/Livonia-Save-Our-Youth-Task-Force/173106307017" title="Connect with us on Facebook" target="_blank">Connect with us<br /><span class="highlight-fade">on Facebook</span></a>
            <a class="btn-highlight" href="/contact/e-mail-updates/" title="Sign up for our mailing list">Sign up<br /><span class="highlight-fade">for our mailing list</span></a>
        </div>
    
    </div><!--/#container-->
    
    <div id="footer-wide">
    	<div id="footer">
    		<div class="footer-main">
            	<a href="http://www.livoniasaveouryouth.org"><img class="icon" src="http://www.livoniasaveouryouth.org/wp-content/themes/save-our-youth/images/logo-footer.png" title="Livonia Save Our Youth Coalition" /></a><br />
                <p>Livonia City Hall Annex, 33000 Civic Center Drive, Livonia, <abbr title="Michigan">MI</abbr> 48154<br />(734) 338-9580 &bull; <a href="mailto:info@livoniasaveouryouth.org">e-mail</a> &bull; <a href="http://www.livoniasaveouryouth.org/site-map">site map</a> &bull; Website Maintained by <a href="http://websitesbythemenu.com/" target="_blank" title="Websites by the Menu">Websites by the Menu</a></p> 

<p style="font-size:10px">Funding provided by (or -&nbsp;<b>in part by</b>)&nbsp;the&nbsp;Detroit Wayne Mental Health Authority (DWMHA)</p>
            </div>
            <div class="footer-sub">
            	<a class="btn-green" href="http://www.livoniasaveouryouth.org/programs-and-events" title="Come to an event">Come to an Event</a><br />
                <a class="btn-orange" href="http://www.livoniasaveouryouth.org/programs-and-events/coalition-meetings" title="Join our meetings">Join Our Meetings</a><br />
                <a class="btn-blue" href="http://www.livoniasaveouryouth.org/programs-and-events/call-for-volunteers" title="Volunteer">Volunteer with Us</a>
            </div>
            <div class="footer-sub">
            	<br /><br />
            	<a href="http://www.facebook.com/pages/Livonia-Save-Our-Youth-Task-Force/173106307017" target="_blank"><img class="icon" src="http://www.livoniasaveouryouth.org/wp-content/themes/save-our-youth/images/icon-facebook.png" title="Find us on Facebook" /></a><br />
                <a href="https://twitter.com/#!/lsoyc" target="_blank"><img class="icon" src="http://www.livoniasaveouryouth.org/wp-content/themes/save-our-youth/images/icon-twitter.png" title="Follow us on Twitter" /></a>
            </div>
        </div><!--/#footer-->
    </div><!--/#footer-wide-->

</body>
</html>
 

